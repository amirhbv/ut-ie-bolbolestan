package ir.ut.bolbolestan;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;

import ir.ut.bolbolestan.apiServices.http.ApiService;
import ir.ut.bolbolestan.exception.OfferingNotFoundException;
import ir.ut.bolbolestan.services.Offering;
import ir.ut.bolbolestan.services.Student;
import ir.ut.bolbolestan.services.TakenCourse;

public class BolbolestanInitializer {

    public static void initialize() {
        try {
            initializeCourses();
            initializeStudents();
        } catch (Exception ignored) {}
    }

    private static void initializeCourses() {
        try {
            JSONArray jArray = new JSONArray(ApiService.get("http://138.197.181.131:5200/api/courses"));

            ArrayList<Offering> offerings = new ArrayList<>();
            for (int i = 0; i < jArray.length(); i++) {
                offerings.add(Offering.deserialize(jArray.getJSONObject(i)));
            }

            for (Offering offering : offerings) {
                offering.addToDB();
            }

            for (Offering offering : offerings) {
                offering.addPrerequisitesToDB();
            }

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private static void initializeStudents() {
        try {
            JSONArray jArray = new JSONArray(ApiService.get("http://138.197.181.131:5200/api/students"));

            for (int i = 0; i < jArray.length(); i++) {
                Student student = Student.deserialize(jArray.getJSONObject(i));
                student.addToDB();
                for (TakenCourse takenCourse : BolbolestanInitializer.getGrades(student.getId())) {
                    student.addTakenCourseToDB(takenCourse);
                }
            }

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private static ArrayList<TakenCourse> getGrades(String studentId) {
        ArrayList<TakenCourse> takenCourses = new ArrayList<>();
        try {
            JSONArray jArray = new JSONArray(ApiService.get("http://138.197.181.131:5200/api/grades/" + studentId));
            for (int i = 0; i < jArray.length(); i++) {
                try {
                    TakenCourse takenCourse = TakenCourse.deserialize(jArray.getJSONObject(i));
                    Map<String, Object> offeringData = Offering.getCourseByCode(takenCourse.getCode());
                    takenCourse.setUnits((Integer) offeringData.get("units"));
                    takenCourse.setName((String) offeringData.get("name"));
                    takenCourses.add(takenCourse);
                } catch (OfferingNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return takenCourses;
    }

}
