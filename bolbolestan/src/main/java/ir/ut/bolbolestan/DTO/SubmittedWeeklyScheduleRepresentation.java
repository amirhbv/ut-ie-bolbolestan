package ir.ut.bolbolestan.DTO;

import java.util.ArrayList;

public class SubmittedWeeklyScheduleRepresentation {
    public ArrayList<WeeklyScheduleOfferingRepresentation> submitted;

    public SubmittedWeeklyScheduleRepresentation(
        ArrayList<WeeklyScheduleOfferingRepresentation> submitted
    ) {
        this.submitted = submitted;
    }
}
