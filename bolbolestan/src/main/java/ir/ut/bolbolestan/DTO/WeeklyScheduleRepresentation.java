package ir.ut.bolbolestan.DTO;

import java.util.ArrayList;

public class WeeklyScheduleRepresentation extends SubmittedWeeklyScheduleRepresentation {
    public final ArrayList<WeeklyScheduleOfferingRepresentation> waitList;

    public WeeklyScheduleRepresentation(
        ArrayList<WeeklyScheduleOfferingRepresentation> submitted,
        ArrayList<WeeklyScheduleOfferingRepresentation> waitList
    ) {
        super(submitted);
        this.waitList = waitList;
    }
}
