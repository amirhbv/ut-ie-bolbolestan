package ir.ut.bolbolestan.DTO;

public class SignUpDTO {
    public String name;
    public String lastName;
    public String studentId;
    public String birthDate;
    public String major;
    public String faculty;
    public String level;
    public String email;
    public String password;
}
