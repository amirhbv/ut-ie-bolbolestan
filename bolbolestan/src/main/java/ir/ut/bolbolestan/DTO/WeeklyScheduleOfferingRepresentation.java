package ir.ut.bolbolestan.DTO;

import ir.ut.bolbolestan.services.ClassTime;
import ir.ut.bolbolestan.services.StudentWaitListRecord;
import ir.ut.bolbolestan.services.WeeklyScheduleOffering;

public class WeeklyScheduleOfferingRepresentation {
    public final String name;
    public final ClassTime classTime;
    public final String instructor;
    public final String code;
    public final String status;
    public final Integer units;
    public final String type;

    public WeeklyScheduleOfferingRepresentation(WeeklyScheduleOffering weeklyScheduleOffering) {
        this.name = weeklyScheduleOffering.getOffering().getName();
        this.classTime = weeklyScheduleOffering.getOffering().getClassTime();
        this.instructor = weeklyScheduleOffering.getOffering().getInstructor();
        this.code = weeklyScheduleOffering.getOffering().getUniqueCode();
        this.status = weeklyScheduleOffering.getIsFinalized() ? "finalized" : weeklyScheduleOffering.getGoingToBeRemoved() ? "toBeRemoved" : "pending";
        this.units = weeklyScheduleOffering.getOffering().getUnits();
        this.type = weeklyScheduleOffering.getOffering().getType();
    }

    public WeeklyScheduleOfferingRepresentation(StudentWaitListRecord record) {
        this.name = record.getOffering().getName();
        this.classTime = record.getOffering().getClassTime();
        this.instructor = record.getOffering().getInstructor();
        this.code = record.getOffering().getUniqueCode();
        this.status = record.getStatus();
        this.units = record.getOffering().getUnits();
        this.type = record.getOffering().getType();
    }
}
