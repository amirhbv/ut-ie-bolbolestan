package ir.ut.bolbolestan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ir.ut.bolbolestan.DTO.SignUpDTO;
import ir.ut.bolbolestan.DTO.SubmittedWeeklyScheduleRepresentation;
import ir.ut.bolbolestan.DTO.WeeklyScheduleRepresentation;
import ir.ut.bolbolestan.exception.BadRequestException;
import ir.ut.bolbolestan.exception.BolbolestanException;
import ir.ut.bolbolestan.exception.OfferingNotFoundException;
import ir.ut.bolbolestan.exception.StudentNotFoundException;
import ir.ut.bolbolestan.exception.StudentNotLoggedInException;
import ir.ut.bolbolestan.exception.WrongOfferingTypeException;
import ir.ut.bolbolestan.services.Offering;
import ir.ut.bolbolestan.services.Student;
import ir.ut.bolbolestan.services.TakenCourse;
import ir.ut.bolbolestan.services.WaitListHandler;

public class Bolbolestan {
    private Student loggedInStudent = null;

    public Bolbolestan() {}

    public Bolbolestan(Student student) {
        this.loggedInStudent = student;
    }

    public static Bolbolestan getInstance() {
        return new Bolbolestan();
    }

    public static Bolbolestan getInstance(Student student) {
        return new Bolbolestan(student);
    }

    private Student getLoggedInStudent() throws StudentNotLoggedInException {
        if (this.loggedInStudent == null) {
            throw new StudentNotLoggedInException();
        }

        return this.loggedInStudent;
    }

    public String login(String email, String password) throws StudentNotFoundException {
        Student student = Student.getByEmailAndPassword(email, password);
        return student.generateJWT();
    }

    public void forgetPassword(String email) throws StudentNotFoundException {
        this.getStudentByEmail(email).sendForgetPasswordEmail();
    }

    public void changePassword(String password) throws StudentNotLoggedInException {
        Student student = this.getLoggedInStudent();
        student.changePassword(password);
    }

    public ArrayList<Offering> getOfferings(String type, String searchedPhrase) throws WrongOfferingTypeException {
        return Offering.getListFromDB(type, searchedPhrase);
    }

    public String signUp(SignUpDTO data) throws BadRequestException {
        Student student = Student.signUp(data);
        return student.generateJWT();
    }

    public Map<String, String> getProfileInfo() throws StudentNotLoggedInException {
        Student student = this.getLoggedInStudent();
        return student.getProfileInfoFromDB();
    }

    public HashMap<Integer, ArrayList<TakenCourse>> getTakenCourseByTerm() throws StudentNotLoggedInException {
        Student student = this.getLoggedInStudent();
        return student.getTakenCourseByTermFromDB();
    }

    public Offering getOffering(String code) throws OfferingNotFoundException {
        return Offering.getByCode(code);
    }

    public Student getStudent(String studentId) throws StudentNotFoundException {
        return Student.getById(studentId);
    }

    public Student getStudentByEmail(String email) throws StudentNotFoundException {
        return Student.getByEmail(email);
    }

    public void addToLoggedInStudentWeeklySchedule(String code) throws BolbolestanException {
        Student student = this.getLoggedInStudent();
        Offering offering = getOffering(code);
        student.addToWeeklySchedule(offering);
    }

    public void removeFromLoggedInStudentWeeklyschedule(String code) throws BolbolestanException {
        Student student = getLoggedInStudent();
        Offering offering = getOffering(code);
        student.removeFromWeeklySchedule(offering);
    }

    public void removeFromLoggedInStudentWaitList(String code) throws BolbolestanException {
        Student student = getLoggedInStudent();
        Offering offering = getOffering(code);
        WaitListHandler.getInstance(student).removeFromWaitList(offering, student);
    }

    public void addToLoggedInStudentWaitList(String code) throws BolbolestanException {
        Student student = getLoggedInStudent();
        Offering offering = getOffering(code);
        WaitListHandler.getInstance(student).addToWaitList(offering, student);
    }

    public void finalizeLoggedInStudentSchedule() throws BolbolestanException {
        Student student = getLoggedInStudent();
        student.finalizeSchedule();
    }

    public WeeklyScheduleRepresentation getLoggedInStudentScheduleRepresentation() throws StudentNotLoggedInException {
        return this.getScheduleRepresentation(getLoggedInStudent());
    }

    public SubmittedWeeklyScheduleRepresentation getLoggedInStudentSubmittedScheduleRepresentation() throws StudentNotLoggedInException {
        return this.getSubmittedScheduleRepresentation(getLoggedInStudent());
    }

    public WeeklyScheduleRepresentation getScheduleRepresentation(Student student) {
        return student.getScheduleRepresentation();
    }

    public SubmittedWeeklyScheduleRepresentation getSubmittedScheduleRepresentation(Student student) {
        return student.getSubmittedScheduleRepresentation();
    }

    public void processWaitList() {
        System.out.println("running runnable");
        WaitListHandler.getInstance().processWaitList();
    }
}
