package ir.ut.bolbolestan.controllers;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ir.ut.bolbolestan.Bolbolestan;
import ir.ut.bolbolestan.DTO.ForgetPasswordDTO;
import ir.ut.bolbolestan.exception.StudentNotFoundException;


@RestController
@RequestMapping("/forget-password")
public class ForgetPasswordController {

    @PostMapping("")
    public void forgetPassword(
        @RequestBody ForgetPasswordDTO data,
        final HttpServletResponse response
    ) throws StudentNotFoundException {

        Bolbolestan.getInstance().forgetPassword(data.email);
        response.setStatus(HttpStatus.OK.value());
    }
}
