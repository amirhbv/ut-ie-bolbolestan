package ir.ut.bolbolestan.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ir.ut.bolbolestan.Bolbolestan;
import ir.ut.bolbolestan.DTO.LoginDTO;
import ir.ut.bolbolestan.exception.StudentNotFoundException;


@RestController
@RequestMapping("/login")
public class LoginController {

    @PostMapping("")
    public Map<String, String> login(@RequestBody LoginDTO data, final HttpServletResponse response) throws StudentNotFoundException {
        return Map.of("token", Bolbolestan.getInstance().login(data.email, data.password));
    }
}
