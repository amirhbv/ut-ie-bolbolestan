package ir.ut.bolbolestan.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ir.ut.bolbolestan.Bolbolestan;
import ir.ut.bolbolestan.exception.WrongOfferingTypeException;
import ir.ut.bolbolestan.services.Offering;


@RestController
@RequestMapping("/api/offerings")
public class OfferingsController {
    @GetMapping("")
    public ArrayList<Offering> getOfferings(
        @RequestParam(required = false) String type,
        @RequestParam(required = false) String q
    ) throws WrongOfferingTypeException {
        return Bolbolestan.getInstance().getOfferings(type, q);
    }
}
