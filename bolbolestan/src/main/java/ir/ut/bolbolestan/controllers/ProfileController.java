package ir.ut.bolbolestan.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ir.ut.bolbolestan.Bolbolestan;
import ir.ut.bolbolestan.exception.StudentNotLoggedInException;
import ir.ut.bolbolestan.services.Student;


@RestController
@RequestMapping("/api/profile")
public class ProfileController {

    @GetMapping("")
    public Map<String, String> getProfile(final HttpServletRequest request) throws StudentNotLoggedInException {
        Student student = (Student) request.getAttribute("student");
        return Bolbolestan.getInstance(student).getProfileInfo();
    }

}
