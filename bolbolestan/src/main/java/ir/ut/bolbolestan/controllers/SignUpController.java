package ir.ut.bolbolestan.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ir.ut.bolbolestan.Bolbolestan;
import ir.ut.bolbolestan.DTO.SignUpDTO;
import ir.ut.bolbolestan.exception.BadRequestException;


@RestController
@RequestMapping("/signup")
public class SignUpController{

    @PostMapping("")
    public Map<String, String> signup(@RequestBody SignUpDTO data, final HttpServletResponse response) throws BadRequestException {
        return Map.of("token", Bolbolestan.getInstance().signUp(data));
    }
}
