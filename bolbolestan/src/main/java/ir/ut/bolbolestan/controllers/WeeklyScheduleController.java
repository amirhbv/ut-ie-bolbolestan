package ir.ut.bolbolestan.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ir.ut.bolbolestan.Bolbolestan;
import ir.ut.bolbolestan.DTO.CourseCodeDTO;
import ir.ut.bolbolestan.DTO.SubmittedWeeklyScheduleRepresentation;
import ir.ut.bolbolestan.exception.BolbolestanException;
import ir.ut.bolbolestan.exception.StudentNotLoggedInException;
import ir.ut.bolbolestan.services.Student;


@RestController
@RequestMapping("/api/weekly-schedule")
public class WeeklyScheduleController{

    @GetMapping("")
    public SubmittedWeeklyScheduleRepresentation getWeeklySchedule(
        @RequestParam(required = false, defaultValue = "false") boolean submitted,
        final HttpServletRequest request
    ) throws StudentNotLoggedInException {

        Student student = (Student) request.getAttribute("student");
        Bolbolestan bolbolestan = Bolbolestan.getInstance(student);
        if (submitted) {
            return bolbolestan.getLoggedInStudentSubmittedScheduleRepresentation();
        }
        return bolbolestan.getLoggedInStudentScheduleRepresentation();
    }

    @PostMapping("")
    public void addToWeeklySchedule(@RequestBody CourseCodeDTO data, final HttpServletRequest request,
            final HttpServletResponse response) throws BolbolestanException {

        Student student = (Student) request.getAttribute("student");
        Bolbolestan.getInstance(student).addToLoggedInStudentWeeklySchedule(data.code);
        response.setStatus(HttpStatus.OK.value());
    }

    @PostMapping("/wait-list")
    public void addToWaitList(@RequestBody CourseCodeDTO data, final HttpServletRequest request,
            final HttpServletResponse response) throws BolbolestanException {

        Student student = (Student) request.getAttribute("student");
        Bolbolestan.getInstance(student).addToLoggedInStudentWaitList(data.code);
        response.setStatus(HttpStatus.OK.value());
    }

    @DeleteMapping("/{code}")
    public void removeFromWeeklySchedule(@PathVariable String code, final HttpServletRequest request,
            final HttpServletResponse response) throws BolbolestanException {

        Student student = (Student) request.getAttribute("student");
        Bolbolestan.getInstance(student).removeFromLoggedInStudentWeeklyschedule(code);
        response.setStatus(HttpStatus.NO_CONTENT.value());
    }

    @DeleteMapping("/wait-list/{code}")
    public void removeFromWaitList(@PathVariable String code, final HttpServletRequest request,
            final HttpServletResponse response) throws BolbolestanException {

        Student student = (Student) request.getAttribute("student");
        Bolbolestan.getInstance(student).removeFromLoggedInStudentWaitList(code);
        response.setStatus(HttpStatus.NO_CONTENT.value());
    }

    @PostMapping("/submit")
    public void finalizeSchedule(final HttpServletRequest request) throws BolbolestanException {

        Student student = (Student) request.getAttribute("student");
        Bolbolestan.getInstance(student).finalizeLoggedInStudentSchedule();
    }

}
