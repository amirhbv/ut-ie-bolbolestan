package ir.ut.bolbolestan.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ir.ut.bolbolestan.Bolbolestan;
import ir.ut.bolbolestan.DTO.ChangePasswordDTO;
import ir.ut.bolbolestan.exception.StudentNotLoggedInException;
import ir.ut.bolbolestan.services.Student;


@RestController
@RequestMapping("/api/change-password")
public class ChangePasswordController {

    @PostMapping("")
    public void changePassword(
        @RequestBody ChangePasswordDTO data,
        final HttpServletRequest request,
        final HttpServletResponse response
    ) throws StudentNotLoggedInException {

        Student student = (Student) request.getAttribute("student");
        Bolbolestan.getInstance(student).changePassword(data.password);
        response.setStatus(HttpStatus.OK.value());

    }
}

