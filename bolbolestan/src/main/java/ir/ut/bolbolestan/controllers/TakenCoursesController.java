package ir.ut.bolbolestan.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ir.ut.bolbolestan.Bolbolestan;
import ir.ut.bolbolestan.exception.StudentNotLoggedInException;
import ir.ut.bolbolestan.services.Student;
import ir.ut.bolbolestan.services.TakenCourse;


@RestController
@RequestMapping("/api/taken-courses")
public class TakenCoursesController {

    @GetMapping("")
    public HashMap<Integer, ArrayList<TakenCourse>> getTakenCourses(
            final HttpServletRequest request) throws StudentNotLoggedInException {

        Student student = (Student) request.getAttribute("student");
        return Bolbolestan.getInstance(student).getTakenCourseByTerm();
    }
}
