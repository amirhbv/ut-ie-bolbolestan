package ir.ut.bolbolestan.exception;;

public class CapacityException extends BadRequestException {

    private static final long serialVersionUID = 6617892433069505650L;

    public CapacityException(String code) {
        super(String.format("درس با کد %s پر شده است.", code));
    }

}
