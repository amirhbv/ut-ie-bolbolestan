package ir.ut.bolbolestan.exception;

public class WrongOfferingTypeException extends BadRequestException {
    public WrongOfferingTypeException() {
        super("Offering type should be chosen from this list: ['Asli', 'Paaye', 'Takhasosi', 'Umumi']");
    }
}
