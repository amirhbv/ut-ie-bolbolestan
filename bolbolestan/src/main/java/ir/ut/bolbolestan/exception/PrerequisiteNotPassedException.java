package ir.ut.bolbolestan.exception;;

public class PrerequisiteNotPassedException extends BadRequestException {
    /**
     *
     */
    private static final long serialVersionUID = 8061698571570160996L;

    public PrerequisiteNotPassedException(String prerequisiteCode, String code) {
        super("پیش‌نیاز با کد " + prerequisiteCode + " برای درس با کد  " + code + "هنوز پاس نشده است.");
    }
}
