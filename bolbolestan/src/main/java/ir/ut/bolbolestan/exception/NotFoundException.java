package ir.ut.bolbolestan.exception;


public class NotFoundException extends BolbolestanException{
    public NotFoundException(String msg) {
        super(msg);
    }
}
