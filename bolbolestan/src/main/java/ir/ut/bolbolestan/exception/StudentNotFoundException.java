package ir.ut.bolbolestan.exception;;

public class StudentNotFoundException extends NotFoundException {
    /**
     *
     */
    private static final long serialVersionUID = 5049263369115389775L;

    public StudentNotFoundException() {
        super("ایمیل یا پسورد اشتباه است.");
    }
}
