package ir.ut.bolbolestan.exception;

public class StudentNotInWaitListException extends BadRequestException {
    public StudentNotInWaitListException(String studentId, String offeringCode) {
        super("دانشجو با شماره دانشجویی " + studentId + " در لیست انتظار درس با کد " + offeringCode + " قرار ندارد.");
    }
}
