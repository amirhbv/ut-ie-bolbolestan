package ir.ut.bolbolestan.exception;;

public class BadRequestException extends BolbolestanException {

    public BadRequestException(String msg) {
        super(msg);
    }

    /**
     *
     */
    private static final long serialVersionUID = 2495960359518076897L;

}
