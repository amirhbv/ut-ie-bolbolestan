package ir.ut.bolbolestan.exception;;

public class CourseAlreadyTakenException extends BadRequestException {
    public CourseAlreadyTakenException(String code) {
        super(String.format("درس با کد %s قبلا پاس شده است.", code));
    }
}
