package ir.ut.bolbolestan.exception;;

public class PageNotFoundException extends NotFoundException {

    public PageNotFoundException(String page) {
        super("Page \"" + page + "\" not found.");
    }

    /**
     *
     */
    private static final long serialVersionUID = 2495960359518076897L;

}
