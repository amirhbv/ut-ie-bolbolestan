package ir.ut.bolbolestan.exception;;

public class StudentAlreadyInWaitListException extends BadRequestException {
    public StudentAlreadyInWaitListException(String studentId, String offeringCode) {
        super("دانشجو با شماره دانشجویی " + studentId + " از قبل در لیست انتظار درس با کد " + offeringCode + " قرار دارد.");
    }
}
