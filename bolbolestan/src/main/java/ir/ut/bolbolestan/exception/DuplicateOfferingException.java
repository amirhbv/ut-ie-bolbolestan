package ir.ut.bolbolestan.exception;;

public class DuplicateOfferingException extends BadRequestException {

    /**
     *
     */
    private static final long serialVersionUID = 588035026244437437L;

    public DuplicateOfferingException() {
        super("There is another offering with this code.");
    }

}
