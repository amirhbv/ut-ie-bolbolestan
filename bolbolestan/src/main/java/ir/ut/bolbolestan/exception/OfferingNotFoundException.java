package ir.ut.bolbolestan.exception;;

public class OfferingNotFoundException extends NotFoundException {
    /**
     *
     */
    private static final long serialVersionUID = 938524952791389648L;

    public OfferingNotFoundException(String code) {
        super("No offering found with the given code. " + code);
    }
}
