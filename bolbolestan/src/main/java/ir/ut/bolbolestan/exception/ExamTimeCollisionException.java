package ir.ut.bolbolestan.exception;;

public class ExamTimeCollisionException extends BadRequestException {

    private static final long serialVersionUID = 6617892433069505650L;

    public ExamTimeCollisionException(String code1, String code2) {
        super(String.format("زمان امتحان درس با کد %s با درس با کد %s تداخل دارد.", code1, code2));
    }

}
