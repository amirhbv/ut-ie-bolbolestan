package ir.ut.bolbolestan.exception;;

public class DuplicateStudentException extends BadRequestException {

    /**
     *
     */
    private static final long serialVersionUID = 588035026244437437L;

    public DuplicateStudentException() {
        super("There is another student with this studentId.");
    }

}
