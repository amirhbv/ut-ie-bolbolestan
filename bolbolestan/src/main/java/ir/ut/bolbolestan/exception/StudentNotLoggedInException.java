package ir.ut.bolbolestan.exception;;

public class StudentNotLoggedInException extends BolbolestanException {
    public StudentNotLoggedInException() {
        super("Student not logged in!!");
    }
}
