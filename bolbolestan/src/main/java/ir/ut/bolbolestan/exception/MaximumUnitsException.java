package ir.ut.bolbolestan.exception;;

public class MaximumUnitsException extends BadRequestException {

    private static final long serialVersionUID = 6617892433069505650L;

    public MaximumUnitsException() {
        super("شما می‌توانید حداکثر ۲۰ واحد ثبت کنید.");
    }

}
