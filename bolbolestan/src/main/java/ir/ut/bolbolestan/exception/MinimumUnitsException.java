package ir.ut.bolbolestan.exception;;

public class MinimumUnitsException extends BadRequestException {

    private static final long serialVersionUID = 6617892433069505650L;

    public MinimumUnitsException() {
        super("شما باید حداقل ۱۲ واحد انتخاب کنید تا بتوانید ثبت کنید.");
    }

}
