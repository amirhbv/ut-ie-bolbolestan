package ir.ut.bolbolestan;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

import ir.ut.bolbolestan.services.WaitListTask;

@ServletComponentScan
@SpringBootApplication
public class BolbolestanApplication {

	public static void main(String[] args) {
        SpringApplication.run(BolbolestanApplication.class, args);
        BolbolestanInitializer.initialize();

		ScheduledExecutorService waitListScheduler = Executors.newSingleThreadScheduledExecutor();
		waitListScheduler.scheduleAtFixedRate(new WaitListTask(), 0, 15, TimeUnit.MINUTES);
	}

}
