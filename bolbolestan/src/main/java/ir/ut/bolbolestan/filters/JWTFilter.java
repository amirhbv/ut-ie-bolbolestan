package ir.ut.bolbolestan.filters;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;

import io.jsonwebtoken.JwtException;
import ir.ut.bolbolestan.exception.StudentNotFoundException;
import ir.ut.bolbolestan.services.JWTHandler;
import ir.ut.bolbolestan.services.Student;


@WebFilter(urlPatterns = "/api/*")
public class JWTFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        System.out.println("JWT FILTER");

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        System.out.println("print 1");
        boolean statusIsSet = false;
        try {
            System.out.println("print 2");
            String authorization = httpRequest.getHeader("Authorization");
            if (authorization == null || authorization == "") {
                System.out.println("print 3");
                httpResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
                statusIsSet = true;
                System.out.println("JWT FILTER 401");
            } else {
                System.out.println("print 4");
                Map<String, String> payload = JWTHandler.validate(authorization);
                request.setAttribute("student", Student.getById(payload.get("studentId")));
            }
        } catch (JwtException e) {
            System.out.println("print 5");
            e.printStackTrace();
            httpResponse.setStatus(HttpStatus.FORBIDDEN.value());
            statusIsSet = true;
            System.out.println("JWT FILTER 403");
        } catch (StudentNotFoundException e) {
            System.out.println("print 6");
            e.printStackTrace();
            httpResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
            statusIsSet = true;
            System.out.println("JWT FILTER 401");
        }

        if (statusIsSet) {
            return;
        }
        System.out.println("doing filter");
        chain.doFilter(request, response);

        System.out.println("JWT FILTER after");
    }

}
