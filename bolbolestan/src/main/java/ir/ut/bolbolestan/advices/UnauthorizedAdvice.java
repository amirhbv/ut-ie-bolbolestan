package ir.ut.bolbolestan.advices;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import ir.ut.bolbolestan.exception.StudentNotLoggedInException;

@EnableWebMvc
@ControllerAdvice
public class UnauthorizedAdvice {
    @ResponseBody
    @ExceptionHandler(StudentNotLoggedInException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    Map<String, String> unauthorizedHandler(StudentNotLoggedInException ex, WebRequest request) {
        return Map.of("error", ex.getMessage());
    }
}
