package ir.ut.bolbolestan.advices;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import ir.ut.bolbolestan.exception.NotFoundException;

@ControllerAdvice
public class NotFoundAdvice {
    @ResponseBody
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    Map<String, String> notFoundHandler(NotFoundException ex) {
        return Map.of("error", ex.getMessage());
    }
}
