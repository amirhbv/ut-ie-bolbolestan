package ir.ut.bolbolestan.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class DatabaseHandler {

    private final static String DRIVER_PREFIX = "jdbc:";
    private final static String MYSQL_URL = "mysql://db-svc.nima-ns:3306/bolbolestan";
    private final static String MYSQL_FLAGS = "?useUnicode=yes&characterEncoding=UTF-8&allowPublicKeyRetrieval=true&useSSL=false";
    private final static String USERNAME = System.getenv("DB_USERNAME");
    private final static String PASSWORD = System.getenv("DB_PASSWORD");

    private static ComboPooledDataSource initializeDataSource() {
        System.out.println("DB USER: " + USERNAME);
        ComboPooledDataSource dataSource = new ComboPooledDataSource();

        dataSource.setJdbcUrl(DRIVER_PREFIX + MYSQL_URL + MYSQL_FLAGS);
        dataSource.setUser(USERNAME);
        dataSource.setPassword(PASSWORD);

        dataSource.setInitialPoolSize(5);
        dataSource.setMinPoolSize(5);
        dataSource.setAcquireIncrement(5);
        dataSource.setMaxPoolSize(20);
        dataSource.setMaxStatements(100);

        return dataSource;
    }

    private final static ComboPooledDataSource dataSource = initializeDataSource();

    public static ComboPooledDataSource getDataSource() {
        return dataSource;
    }

    public static void closeStatement(Connection connection,
                                      PreparedStatement statement)
            throws SQLException {
        statement.close();
        // connection.close();
    }

    public static void closeResultSet(Connection connection,
                                      PreparedStatement statement,
                                      ResultSet resultSet)
            throws SQLException {
        resultSet.close();
        closeStatement(connection, statement);
    }
}
