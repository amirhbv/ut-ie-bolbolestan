package ir.ut.bolbolestan.services;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.crypto.SecretKey;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

public class JWTHandler {
    private static SecretKey secretKey = Keys.hmacShaKeyFor("bolbolestanbolbolestanbolbolestan".getBytes(StandardCharsets.UTF_8));

    public static String generate(Map<String, String> claims, long expirationSeconds) {
        return Jwts.builder()
            .signWith(secretKey)
            .setSubject("bolbol")
            .setIssuer("Bolbolestan")
            .setIssuedAt(new Date())
            .setExpiration(new Date(System.currentTimeMillis() + expirationSeconds * 1000))
            .setId(UUID.randomUUID().toString())
            .setClaims(claims)
            .compact();
    }

    public static Map<String, String> validate(String token) {
        Jws<Claims> claims = Jwts.parserBuilder()
            .setSigningKey(secretKey)
            .build()
            .parseClaimsJws(token);

        Map<String, String> payload = new HashMap<>();
        claims.getBody().forEach((key, value) -> {
            payload.put(key, value.toString());
        });

        return payload;
    }
}
