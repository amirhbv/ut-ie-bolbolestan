package ir.ut.bolbolestan.services;

import ir.ut.bolbolestan.DTO.WeeklyScheduleOfferingRepresentation;
import ir.ut.bolbolestan.database.DatabaseHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class WeeklyScheduleOffering {
    private final Offering offering;
    private Boolean isFinalized;
    private Boolean isGoingToBeRemoved = false;

    public WeeklyScheduleOffering(Offering offering, boolean isFinalized) {
        this.offering = offering;
        this.isFinalized = isFinalized;
    }

    public WeeklyScheduleOffering(Offering offering, boolean isFinalized, boolean isGoingToBeRemoved) {
        this.offering = offering;
        this.isFinalized = isFinalized;
        this.isGoingToBeRemoved = isGoingToBeRemoved;
    }


    public Offering getOffering() {
        return offering;
    }

    public Boolean getIsFinalized() {
        return this.isFinalized;
    }

    public void updateStatusInDb(Student student, String status) {
        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                "UPDATE WeeklyScheduleOffering SET status = ? " +
                    "WHERE offeringCode = ? AND offeringClassCode = ? AND studentId = ?"
            );
            statement.setString(1, status);
            statement.setString(2, offering.getCode());
            statement.setString(3, offering.getClassCode());
            statement.setString(4, student.getId());
            statement.executeUpdate();
            DatabaseHandler.closeStatement(connection, statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Boolean getGoingToBeRemoved() {
        return this.isGoingToBeRemoved;
    }

    public void finalizeOffering() {
        if (!this.isFinalized) {
            this.offering.increaseSignedUp();
        }
        this.isFinalized = true;
    }

    public WeeklyScheduleOfferingRepresentation getRepresentation() {
        return new WeeklyScheduleOfferingRepresentation(this);
    }

        public static WeeklyScheduleOffering getWeeklyScheduleOfferingFromResultSet(ResultSet resultSet) throws SQLException {
        return new WeeklyScheduleOffering(
            Offering.getFromResultSet(resultSet),
            resultSet.getString("status").equals("finalized"),
            resultSet.getString("status").equals("toBeRemoved")
        );
    }

    public void deleteWeeklyScheduleOfferingFromDb(Student student) {
        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM WeeklyScheduleOffering " +
                    "WHERE offeringCode = ? AND offeringClassCode = ? AND  studentId = ? "
            );
            statement.setString(1, offering.getCode());
            statement.setString(2, offering.getClassCode());
            statement.setString(3, student.getId());
            statement.executeUpdate();
            DatabaseHandler.closeStatement(connection, statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
