package ir.ut.bolbolestan.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import ir.ut.bolbolestan.DTO.WeeklyScheduleOfferingRepresentation;
import ir.ut.bolbolestan.database.DatabaseHandler;
import ir.ut.bolbolestan.exception.CapacityException;
import ir.ut.bolbolestan.exception.ClassTimeCollisionException;
import ir.ut.bolbolestan.exception.CourseAlreadyTakenException;
import ir.ut.bolbolestan.exception.ExamTimeCollisionException;
import ir.ut.bolbolestan.exception.MaximumUnitsException;
import ir.ut.bolbolestan.exception.MinimumUnitsException;
import ir.ut.bolbolestan.exception.OfferingNotInScheduleException;
import ir.ut.bolbolestan.exception.PrerequisiteNotPassedException;

public class WeeklySchedule {
    private final HashMap<String, WeeklyScheduleOffering> offerings = new HashMap<>();

    public WeeklySchedule(ArrayList<WeeklyScheduleOffering> offerings) {
        for (WeeklyScheduleOffering weeklyScheduleOffering : offerings) {
            this.offerings.put(weeklyScheduleOffering.getOffering().getUniqueCode(), weeklyScheduleOffering);
        }
    }

    public void addOfferingToSchedule(Offering offering, boolean isFinalized, Student student) throws ClassTimeCollisionException, ExamTimeCollisionException {
        WeeklyScheduleOffering result = this.offerings.get(offering.getUniqueCode());
        if (result == null) {
            checkForCollisions(offering);
            try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
                PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO WeeklyScheduleOffering "
                        + "(offeringCode, offeringClassCode, studentId, status, waitList) "
                        + "VALUES (?, ?, ?, ?, ?)"
                );

                statement.setString(1, offering.getCode());
                statement.setString(2, offering.getClassCode());
                statement.setString(3, student.getId());
                statement.setString(4, isFinalized ? "finalized" : "pending");
                statement.setBoolean(5, false);
                statement.executeUpdate();
                DatabaseHandler.closeStatement(connection, statement);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<WeeklyScheduleOffering> getOfferings() {
        return new ArrayList<>(this.offerings.values());
    }

    public void removeOfferingFromSchedule(Offering offering, Student student) throws OfferingNotInScheduleException {
        WeeklyScheduleOffering weeklyScheduleOffering = this.offerings.get(offering.getUniqueCode());
        if (weeklyScheduleOffering == null) {
            throw new OfferingNotInScheduleException();
        }
        if (weeklyScheduleOffering.getIsFinalized()) {
            weeklyScheduleOffering.updateStatusInDb(student, "toBeRemoved");
        } else {
            weeklyScheduleOffering.deleteWeeklyScheduleOfferingFromDb(student);
        }
    }

    public int getSelectedUnits() {
        int result = 0;
        for (WeeklyScheduleOffering weeklyScheduleOffering : this.offerings.values()) {
           result += weeklyScheduleOffering.getOffering().getUnits();
        }
        return result;
    }

    public void finalizeSchedule(Student student) {
        this.finalizeOfferings(student);
    }

    public void finalizeValidations(ArrayList<String> passedCourses) throws MinimumUnitsException, MaximumUnitsException,
        CapacityException, CourseAlreadyTakenException, PrerequisiteNotPassedException {
        this.checkPassedCourses(passedCourses);
        this.checkUnits();
        this.checkPrerequisites(passedCourses);
        this.checkCapacity();
    }

    private void checkPrerequisites(ArrayList<String> passedCourses) throws PrerequisiteNotPassedException {
        for (WeeklyScheduleOffering weeklyScheduleOffering : this.offerings.values()) {
            Offering offering = weeklyScheduleOffering.getOffering();
            for (String prereq : offering.getPrerequisiteCodes()) {
               if (!passedCourses.contains(prereq)) {
                   throw new PrerequisiteNotPassedException(prereq, offering.getCode());
               }
            }
        }
    }

    public void checkPassedCourses(ArrayList<String> passedCourses) throws CourseAlreadyTakenException {
        for (WeeklyScheduleOffering weeklyScheduleOffering : this.offerings.values()) {
            if (!weeklyScheduleOffering.getIsFinalized()) {
                Offering offering = weeklyScheduleOffering.getOffering();
                String code = offering.getCode();
                if (passedCourses.contains(code)) {
                    throw new CourseAlreadyTakenException(code);
                }
            }
        }
    }

    public int getTotalUnits() {
        int totalUnits = 0;
        for (WeeklyScheduleOffering weeklyScheduleOffering : this.offerings.values()) {
            totalUnits += weeklyScheduleOffering.getOffering().getUnits();
        }
        return totalUnits;
    }

    private void checkUnits() throws MinimumUnitsException, MaximumUnitsException {
        int units = 0;
        for (WeeklyScheduleOffering weeklyScheduleOffering : this.offerings.values()) {
            if (!weeklyScheduleOffering.getGoingToBeRemoved()) {
                units += weeklyScheduleOffering.getOffering().getUnits();
            }
        }

        if (units < 12) {
            throw new MinimumUnitsException();
        }

        if (units > 20) {
            throw new MaximumUnitsException();
        }
    }

    private void checkClassTimeCollision(Offering offering) throws ClassTimeCollisionException {
        for (WeeklyScheduleOffering weeklyScheduleOffering : this.offerings.values()) {
            if (
                !weeklyScheduleOffering.getGoingToBeRemoved()
                && weeklyScheduleOffering.getOffering().getClassTime().hasOverlap(offering.getClassTime())
            ) {
                throw new ClassTimeCollisionException(weeklyScheduleOffering.getOffering().getCode(), offering.getCode());
            }
        }
    }

    private void checkExamTimeCollision(Offering offering) throws ExamTimeCollisionException {
        for (WeeklyScheduleOffering weeklyScheduleOffering : this.offerings.values()) {
            if (
                !weeklyScheduleOffering.getGoingToBeRemoved()
                && weeklyScheduleOffering.getOffering().getExamTime().hasOverlap(offering.getExamTime())
            ) {
                throw new ExamTimeCollisionException(weeklyScheduleOffering.getOffering().getCode(), offering.getCode());
            }
        }
    }

    public void checkForCollisions(Offering offering) throws ExamTimeCollisionException, ClassTimeCollisionException {
        checkClassTimeCollision(offering);
        checkExamTimeCollision(offering);
    }

    private void checkCapacity() throws CapacityException {
        for (WeeklyScheduleOffering weeklyScheduleOffering : this.offerings.values()) {
            if (!weeklyScheduleOffering.getIsFinalized()) {
                Offering offering = weeklyScheduleOffering.getOffering();
                if (!offering.hasRemainingCapacity()) {
                    throw new CapacityException(offering.getUniqueCode());
                }
            }
        }
    }

    private void finalizeOfferings(Student student) {
//        ArrayList<Offering> toBeRemovedOfferings = new ArrayList<>();
        for (String code : this.offerings.keySet()) {
            WeeklyScheduleOffering weeklyScheduleOffering = this.offerings.get(code);
            if (!weeklyScheduleOffering.getGoingToBeRemoved()) {
                weeklyScheduleOffering.updateStatusInDb(student, "finalized");
            } else {
                weeklyScheduleOffering.deleteWeeklyScheduleOfferingFromDb(student);
//                toBeRemovedOfferings.add(weeklyScheduleOffering.getOffering());
            }
        }
//        for (Offering offering : toBeRemovedOfferings) {
//            this.offerings.remove(offering.getUniqueCode());
//            offering.decreaseSignedUp();
//        }
    }

    public ArrayList<WeeklyScheduleOfferingRepresentation> getScheduleRepresentation() {
        ArrayList<WeeklyScheduleOfferingRepresentation> result = new ArrayList<>();
        for (WeeklyScheduleOffering weeklyScheduleOffering : this.offerings.values()) {
            result.add(weeklyScheduleOffering.getRepresentation());
        }
        return result;
    }

    public static WeeklySchedule getWeeklyScheduleFromDb(Student student) {
        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {

            PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM WeeklyScheduleOffering W " +
                    "INNER JOIN Offering O ON W.offeringCode = O.code AND W.offeringClassCode = O.classCode " +
                    "INNER JOIN Course C ON O.code = C.code " +
                    "WHERE W.studentId = ? AND waitList = false "
            );
            statement.setString(1, student.getId());

            ResultSet resultSet = statement.executeQuery();
            ArrayList<WeeklyScheduleOffering> offerings = new ArrayList<>();
            while (resultSet.next()) {
                offerings.add(WeeklyScheduleOffering.getWeeklyScheduleOfferingFromResultSet(resultSet));
            }
            DatabaseHandler.closeResultSet(connection, statement, resultSet);
            return new WeeklySchedule(offerings);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
