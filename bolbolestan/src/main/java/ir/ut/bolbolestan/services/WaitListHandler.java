package ir.ut.bolbolestan.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import ir.ut.bolbolestan.DTO.WeeklyScheduleOfferingRepresentation;
import ir.ut.bolbolestan.database.DatabaseHandler;
import ir.ut.bolbolestan.exception.BolbolestanException;
import ir.ut.bolbolestan.exception.ClassTimeCollisionException;
import ir.ut.bolbolestan.exception.CourseAlreadyTakenException;
import ir.ut.bolbolestan.exception.ExamTimeCollisionException;
import ir.ut.bolbolestan.exception.PrerequisiteNotPassedException;
import ir.ut.bolbolestan.exception.StudentAlreadyInWaitListException;
import ir.ut.bolbolestan.exception.StudentNotInWaitListException;

public class WaitListHandler {
    private static WaitListHandler instance = null;
    private final HashMap<Offering, ArrayList<WaitListRecord>> waitList;

    private WaitListHandler() {
        this.waitList = new HashMap<>();
    }

    public static WaitListHandler getInstance() {
        if (WaitListHandler.instance == null) {
            WaitListHandler.instance = new WaitListHandler();
        }
        return WaitListHandler.instance;
    }

    public static WaitListHandler getInstance(Student student) {
        return getStudentWaitListFromDb(student);
    }

    private static WaitListHandler getStudentWaitListFromDb(Student student) {
        WaitListHandler result = new WaitListHandler();
        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM WeeklyScheduleOffering W "
                    + "INNER JOIN Offering O ON W.offeringCode = O.code AND W.offeringClassCode = O.classCode "
                    + "INNER JOIN Course C ON O.code = C.code " + "INNER JOIN Student S ON W.studentId = S.id "
                    + "WHERE W.studentId = ? AND waitList = true ");
            statement.setString(1, student.getId());

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.addWaitListRecordFromResultSet(resultSet);
            }
            DatabaseHandler.closeResultSet(connection, statement, resultSet);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void addWaitListRecordFromResultSet(ResultSet resultSet) throws SQLException {
        Offering offering = Offering.getFromResultSet(resultSet);
        this.waitList.putIfAbsent(offering, new ArrayList<>());
        this.waitList.get(offering).add(WaitListRecord.getFromResultSet(resultSet));
    }

    public void addToWaitList(Offering offering, Student student) throws StudentAlreadyInWaitListException,
        ClassTimeCollisionException, ExamTimeCollisionException {
        student.checkForCollisions(offering);
        this.checkForCollisions(offering, student);
        if (isInOfferingWaitList(offering, student)) {
            throw new StudentAlreadyInWaitListException(student.getId(), offering.getUniqueCode());
        }
        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO WeeklyScheduleOffering "
                    + "(offeringCode, offeringClassCode, studentId, status, waitList) "
                    + "VALUES (?, ?, ?, ?, ?)"
            );

            statement.setString(1, offering.getCode());
            statement.setString(2, offering.getClassCode());
            statement.setString(3, student.getId());
            statement.setString(4, "pending");
            statement.setBoolean(5, true);
            statement.executeUpdate();
            DatabaseHandler.closeStatement(connection, statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeFromWaitList(Offering offering, Student student) throws StudentNotInWaitListException{
        System.out.println("rm " + student.getId() + " " + offering.getUniqueCode());
        ArrayList<StudentWaitListRecord> records = this.getStudentRecords(student);
        for (StudentWaitListRecord record : records) {
            if (record.getOffering().getUniqueCode().equals(offering.getUniqueCode())) {
                if (record.getStatus().equals(StudentWaitListRecord.STATUS_FINALIZED)) {
                    System.out.println("1");
                    record.updateStatusInDb(StudentWaitListRecord.STATUS_TO_BE_REMOVED);
                }
                else if (record.getStatus().equals(StudentWaitListRecord.STATUS_PENDING)) {
                    System.out.println("2");
                    record.deleteWaitListRecordFromDb();
                }
                else {
                    throw new RuntimeException("BAD WAIT LIST REMOVE");
                }
                System.out.println("3");
                return;
            }
        }
        System.out.println("4");
        throw new StudentNotInWaitListException(student.getId(), offering.getUniqueCode());
    }

    public void deleteFromWaitList(Offering offering, WaitListRecord record) {
        this.waitList.get(offering).remove(record);
    }

    private boolean isInOfferingWaitList(Offering offering, Student student) {
        if (!this.waitList.containsKey(offering)) {
            return false;
        }
        for (WaitListRecord waitListRecord : this.waitList.get(offering)) {
            if (waitListRecord.getStudent().getId().equals(student.getId())) {
                return true;
            }
        }
        return false;
    }

    private WaitListRecord getWaitListRecord(Offering offering, Student student) throws StudentNotInWaitListException {
        if (!this.waitList.containsKey(offering)) {
            throw new StudentNotInWaitListException(student.getId(), offering.getUniqueCode());
        }
        for (WaitListRecord waitListRecord : this.waitList.get(offering)) {
            if (waitListRecord.getStudent().getId().equals(student.getId())) {
                return waitListRecord;
            }
        }
        throw new StudentNotInWaitListException(student.getId(), offering.getUniqueCode());
    }

    public ArrayList<StudentWaitListRecord> getStudentRecords(Student student) {
        ArrayList<StudentWaitListRecord> results = new ArrayList<>();
        for (Offering offering : this.waitList.keySet()) {
            try {
                WaitListRecord waitListRecord = this.getWaitListRecord(offering, student);
                results.add(new StudentWaitListRecord(offering, waitListRecord));
            } catch (StudentNotInWaitListException ignored) {
            }
        }
        return results;
    }

    public void checkForCollisions(Offering offering, Student student) throws ClassTimeCollisionException,
        ExamTimeCollisionException {
        ArrayList<StudentWaitListRecord> records = this.getStudentRecords(student);
        for (StudentWaitListRecord record : records) {
            offering.checkCollision(record.getOffering());
        }
    }

    public ArrayList<WeeklyScheduleOfferingRepresentation> getStudentWaitListRepresentation(Student student) {
        ArrayList<WeeklyScheduleOfferingRepresentation> results = new ArrayList<>();
        for (StudentWaitListRecord record : this.getStudentRecords(student)) {
            results.add(new WeeklyScheduleOfferingRepresentation(record));
        }
        return results;
    }

    public void finalizeStudentRecords(Student student) throws CourseAlreadyTakenException, PrerequisiteNotPassedException{
        this.checkPrerequisitesAndAlreadyPassed(student);
        for (StudentWaitListRecord record : this.getStudentRecords(student)) {
            record.finalizeRecord();
        }
    }

    public void checkPrerequisitesAndAlreadyPassed(Student student) throws
        CourseAlreadyTakenException, PrerequisiteNotPassedException {
        ArrayList<String> passedCourses = student.getPassedCoursesFromDB();
        for (StudentWaitListRecord record : this.getStudentRecords(student)) {
            String code = record.getOffering().getCode();
            if (passedCourses.contains(code)) {
                throw new CourseAlreadyTakenException(code);
            }
            for (String prerequisite : record.getOffering().getPrerequisites()) {
                if (!passedCourses.contains(prerequisite)) {
                    throw new PrerequisiteNotPassedException(prerequisite, code);
                }
            }
        }
    }

    public void processWaitList() {
        for (Offering offering : this.waitList.keySet()) {
            ArrayList<WaitListRecord> toBeProcessedRecords = new ArrayList<>();
            for (WaitListRecord record : this.waitList.get(offering)) {
                if (record.isFinalized()) {
                    toBeProcessedRecords.add(record);
                }
            }
            for (WaitListRecord record : toBeProcessedRecords) {
                offering.increaseCapacity();
                offering.increaseSignedUp();
                this.waitList.get(offering).remove(record);
                try {
                    record.getStudent().addToWeeklyScheduleFinalized(offering);
                }
                catch (BolbolestanException e) {
                    throw new RuntimeException("Shouldn't be here");
                }
            }
        }
    }
}
