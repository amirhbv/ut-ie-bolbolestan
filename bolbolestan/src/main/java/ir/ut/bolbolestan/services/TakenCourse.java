package ir.ut.bolbolestan.services;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONObject;

public class TakenCourse {
    private final String code;
    private final float grade;
    private int units;
    private String name;
    private String status;
    private int term;

    public TakenCourse(String code, float grade, int term) {
        this.code = code;
        this.grade = grade;
        this.term = term;
        this.status = this.grade >= 10 ? "passed" : "failed";
    }

    public void setUnits(int units) {
        this.units = units;
    }

    public int getUnits() {
        return units;
    }

    public float getGrade() {
        return this.grade;
    }

    public int getTerm() {
        return this.term;
    }

    public String getCode() {
        return this.code;
    }

    public static TakenCourse deserialize(JSONObject jsonObject) {
        return new TakenCourse(
            jsonObject.getString("code"),
            jsonObject.getInt("grade"),
            jsonObject.getInt("term")
        );
    }

    public String serializeHtmlTableRow() {
        return "<tr>" +
            "<td>" + this.code + "</td>" +
            "<td>" + this.grade + "</td>" +
            "</tr>";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public String getStatus() {
        return this.status;
    }

	public static TakenCourse getFromResultSet(ResultSet resultSet) throws SQLException {
        return new TakenCourse(
            resultSet.getString("code"),
            resultSet.getFloat("grade"),
            resultSet.getInt("term")
        );
	}
}
