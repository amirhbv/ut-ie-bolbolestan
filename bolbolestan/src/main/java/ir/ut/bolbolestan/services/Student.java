package ir.ut.bolbolestan.services;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONObject;

import ir.ut.bolbolestan.DTO.SignUpDTO;
import ir.ut.bolbolestan.DTO.SubmittedWeeklyScheduleRepresentation;
import ir.ut.bolbolestan.DTO.WeeklyScheduleRepresentation;
import ir.ut.bolbolestan.apiServices.http.ApiService;
import ir.ut.bolbolestan.database.DatabaseHandler;
import ir.ut.bolbolestan.exception.BadRequestException;
import ir.ut.bolbolestan.exception.BolbolestanException;
import ir.ut.bolbolestan.exception.CapacityException;
import ir.ut.bolbolestan.exception.ClassTimeCollisionException;
import ir.ut.bolbolestan.exception.CourseAlreadyTakenException;
import ir.ut.bolbolestan.exception.ExamTimeCollisionException;
import ir.ut.bolbolestan.exception.MaximumUnitsException;
import ir.ut.bolbolestan.exception.MinimumUnitsException;
import ir.ut.bolbolestan.exception.OfferingNotInScheduleException;
import ir.ut.bolbolestan.exception.PrerequisiteNotPassedException;
import ir.ut.bolbolestan.exception.StudentNotFoundException;

public class Student {
    private final String id;
    private final String name;
    private final String secondName;
    private final String birthDate;
    private final String field;
    private final String faculty;
    private final String level;
    private final String status;
    private final String img;
    private final String email;
    private final String password;

    private static String hashPassword(String password) {
        return DigestUtils.sha256Hex(password);
    }

    public Student(String id, String name, String secondName, String birthDate,
                   String field, String faculty, String level, String status, String img,
                   String email, String password) {
        this.id = id;
        this.name = name;
        this.secondName = secondName;
        this.birthDate = birthDate;
        this.field = field;
        this.faculty = faculty;
        this.level = level;
        this.status = status;
        this.img = img;
        this.email = email;
        this.password = Student.hashPassword(password);
    }

    public String getId() {
        return id;
    }

    public WeeklySchedule getWeeklySchedule() {
        return WeeklySchedule.getWeeklyScheduleFromDb(this);
    }

    public WeeklyScheduleRepresentation getScheduleRepresentation() {
        return new WeeklyScheduleRepresentation(
            WeeklySchedule.getWeeklyScheduleFromDb(this).getScheduleRepresentation(),
            WaitListHandler.getInstance(this).getStudentWaitListRepresentation(this)
        );
    }

    public SubmittedWeeklyScheduleRepresentation getSubmittedScheduleRepresentation() {
        return new SubmittedWeeklyScheduleRepresentation(
            WeeklySchedule.getWeeklyScheduleFromDb(this).getScheduleRepresentation()
        );
    }

    private float getGpaFromDB() {
        float result = 0;

        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT SUM(grade * units) / SUM(units) AS gpa FROM TakenCourse T INNER JOIN Course C ON C.code = T.code WHERE studentId = ?");
            statement.setString(1, this.id);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getFloat("gpa");
            }

            DatabaseHandler.closeResultSet(connection, statement, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    private Integer getTpuFromDB() {
        Integer result = 0;

        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT SUM(units) AS tpu FROM TakenCourse T INNER JOIN Course C ON C.code = T.code WHERE status = 'passed' AND studentId = ?");
            statement.setString(1, this.id);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("tpu");
            }

            DatabaseHandler.closeResultSet(connection, statement, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public void checkForCollisions(Offering offering) throws ExamTimeCollisionException, ClassTimeCollisionException{
        WeeklySchedule.getWeeklyScheduleFromDb(this).checkForCollisions(offering);
    }

    public void addToWeeklySchedule(Offering offering) throws BolbolestanException {
        WaitListHandler.getInstance(this).checkForCollisions(offering, this);
        WeeklySchedule.getWeeklyScheduleFromDb(this).addOfferingToSchedule(offering, false, this);
    }

    public void addToWeeklyScheduleFinalized(Offering offering) throws BolbolestanException {
        WeeklySchedule.getWeeklyScheduleFromDb(this).addOfferingToSchedule(offering, true, this);
    }

    public void removeFromWeeklySchedule(Offering offering) throws OfferingNotInScheduleException {
        WeeklySchedule.getWeeklyScheduleFromDb(this).removeOfferingFromSchedule(offering, this);
    }

    public void finalizeSchedule() throws MinimumUnitsException, MaximumUnitsException, CapacityException,
        CourseAlreadyTakenException, PrerequisiteNotPassedException {
        WeeklySchedule.getWeeklyScheduleFromDb(this).finalizeValidations(this.getPassedCoursesFromDB());
        WaitListHandler.getInstance(this).finalizeStudentRecords(this);
        WeeklySchedule.getWeeklyScheduleFromDb(this).finalizeSchedule(this);
    }

    public ArrayList<String> getPassedCoursesFromDB() {
        ArrayList<String> result = new ArrayList<>();


        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {

            PreparedStatement statement = connection
                    .prepareStatement("SELECT * FROM TakenCourse WHERE status = 'passed' AND studentId = ?");
            statement.setString(1, this.id);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(resultSet.getString("code"));
            }

            DatabaseHandler.closeResultSet(connection, statement, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public int getSelectedUnits() {
        return WeeklySchedule.getWeeklyScheduleFromDb(this).getSelectedUnits();
    }

    public static Student deserialize(JSONObject jsonObject) {
        return new Student(
            jsonObject.getString("id"),
            jsonObject.getString("name"),
            jsonObject.getString("secondName"),
            jsonObject.getString("birthDate"),
            jsonObject.getString("field"),
            jsonObject.getString("faculty"),
            jsonObject.getString("level"),
            jsonObject.getString("status"),
            jsonObject.getString("img"),
            jsonObject.getString("email"),
            jsonObject.getString("password")
        );
    }

    public Map<String, String> getProfileInfoFromDB() {
        return Map.ofEntries(
            Map.entry("stdId", this.id),
            Map.entry("name", this.name),
            Map.entry("lastName", this.secondName),
            Map.entry("gpa", String.format("%.2f", this.getGpaFromDB())),
            Map.entry("tpu", this.getTpuFromDB().toString()),
            Map.entry("birthDate", this.birthDate),
            Map.entry("faculty", this.faculty),
            Map.entry("field", this.field),
            Map.entry("level", this.level),
            Map.entry("img", this.img),
            Map.entry("status", this.status)
        );
    }

    private ArrayList<TakenCourse> getTakenCoursesFromDB() {
        ArrayList<TakenCourse> result = new ArrayList<>();


        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {

            PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM TakenCourse T INNER JOIN Course C ON C.code = T.code WHERE studentId = ?"
            );
            statement.setString(1, this.id);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                TakenCourse takenCourse = TakenCourse.getFromResultSet(resultSet);
                takenCourse.setUnits(resultSet.getInt("units"));
                takenCourse.setName(resultSet.getString("name"));
                result.add(takenCourse);
            }

            DatabaseHandler.closeResultSet(connection, statement, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public HashMap<Integer, ArrayList<TakenCourse>> getTakenCourseByTermFromDB() {
        HashMap<Integer, ArrayList<TakenCourse>> result = new HashMap<>();
        for (TakenCourse takenCourse : this.getTakenCoursesFromDB()) {
            result.putIfAbsent(takenCourse.getTerm(), new ArrayList<TakenCourse>());
            result.get(takenCourse.getTerm()).add(takenCourse);
        }
        return result;
    }

    public static Student signUp(SignUpDTO data) throws BadRequestException {
        try
        {
            Student student = new Student(
                data.studentId,
                data.name,
                data.lastName,
                data.birthDate,
                data.major,
                data.faculty,
                data.level,
                "مشغول به تحصیل",
                "http://138.197.181.131:5200/img/art.jpg",
                data.email,
                data.password
            );
            student.addToDB(true);
            return student;
        } catch (SQLIntegrityConstraintViolationException e) {
           throw new BadRequestException("ایمیل یا شماره دانشجویی تکراری است.");
        }
    }

    public void changePassword(String password) {

        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                "UPDATE Student SET password = ?"
                    + "WHERE id = ?"
            );

            statement.setString(1, Student.hashPassword(password));
            statement.setString(2, this.id);

            statement.executeUpdate();
            DatabaseHandler.closeStatement(connection, statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void sendForgetPasswordEmail() {
        String requestBody = "{\"email\": \"%s\", \"url\": \"%s\"}";
        String url = String.format(
            "http://127.0.0.1:3000/change-password?token=%s",
            this.generateForgetPasswordJWT()
        );

        try {
            ApiService.post("http://138.197.181.131:5200/api/send_mail", String.format(requestBody, this.email, url));
        } catch (IOException ignored) {}
    }

    public void addToDB() {
        try {
            addToDB(false);
        } catch (SQLException ignored) {}
    }

    public void addToDB(boolean throwException) throws SQLIntegrityConstraintViolationException {

        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO Student "
                    + "(id, name, secondName, birthDate, field, faculty, level, status, img, email, password) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            );

            statement.setString(1, this.id);
            statement.setString(2, this.name);
            statement.setString(3, this.secondName);
            statement.setString(4, this.birthDate);
            statement.setString(5, this.field);
            statement.setString(6, this.faculty);
            statement.setString(7, this.level);
            statement.setString(8, this.status);
            statement.setString(9, this.img);
            statement.setString(10, this.email);
            statement.setString(11, this.password);

            statement.executeUpdate();
            DatabaseHandler.closeStatement(connection, statement);
        } catch (SQLIntegrityConstraintViolationException e) {
            e.printStackTrace();
            if (throwException) {
                throw e;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addTakenCourseToDB(TakenCourse takenCourse) {

        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO TakenCourse "
                + "(studentId, code, grade, term, status) "
                + "VALUES (?, ?, ?, ?, ?)"
            );

            statement.setString(1, this.id);
            statement.setString(2, takenCourse.getCode());
            statement.setFloat(3, takenCourse.getGrade());
            statement.setInt(4, takenCourse.getTerm());
            statement.setString(5, takenCourse.getStatus());

            statement.executeUpdate();
            DatabaseHandler.closeStatement(connection, statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	public static Student getById(String studentId) throws StudentNotFoundException {

        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Student WHERE id = ?");
            statement.setString(1, studentId);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Student result = Student.getFromResultSet(resultSet);
                DatabaseHandler.closeResultSet(connection, statement, resultSet);
                return result;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        throw new StudentNotFoundException();
	}

    public static Student getByEmail(String email) throws StudentNotFoundException {

        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Student WHERE email = ?");
            statement.setString(1, email);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Student result = Student.getFromResultSet(resultSet);
                DatabaseHandler.closeResultSet(connection, statement, resultSet);
                return result;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        throw new StudentNotFoundException();
    }

    public static Student getByEmailAndPassword(String email, String password) throws StudentNotFoundException {

        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Student WHERE email = ? AND password = ?");
            statement.setString(1, email);
            statement.setString(2, Student.hashPassword(password));

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Student result = Student.getFromResultSet(resultSet);
                DatabaseHandler.closeResultSet(connection, statement, resultSet);
                return result;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        throw new StudentNotFoundException();
    }

    public static Student getFromResultSet(ResultSet resultSet) throws SQLException {
        return new Student(
            resultSet.getString("id"),
            resultSet.getString("name"),
            resultSet.getString("secondName"),
            resultSet.getString("birthDate"),
            resultSet.getString("field"),
            resultSet.getString("faculty"),
            resultSet.getString("level"),
            resultSet.getString("status"),
            resultSet.getString("img"),
            resultSet.getString("email"),
            resultSet.getString("password")
        );
    }

    public String generateJWT() {
        return generateJWT(1 * 24 * 60 * 60);
    }

    private String generateForgetPasswordJWT() {
        return generateJWT(10 * 60);
    }

    private String generateJWT(long expirationTime) {
        return JWTHandler.generate(Map.of("studentId", this.id), expirationTime);
    }
}
