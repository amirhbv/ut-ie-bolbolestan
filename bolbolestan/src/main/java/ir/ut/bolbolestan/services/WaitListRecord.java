package ir.ut.bolbolestan.services;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WaitListRecord {
    public static String STATUS_PENDING = "pending";
    public static String STATUS_FINALIZED = "finalized";
    public static String STATUS_TO_BE_REMOVED = "toBeRemoved";

    private final Student student;
    private String status;

    public Student getStudent() {
        return student;
    }

    public WaitListRecord(Student student) {
        this.student = student;
        this.status = WaitListRecord.STATUS_PENDING;
    }

    public WaitListRecord(Student student, String status) {
        this.student = student;
        this.status = status;
    }

    public void submit() {
        this.status = WaitListRecord.STATUS_FINALIZED;
    }

    public String getStatus() {
        return status;
    }

    public void remove() {
        this.status = WaitListRecord.STATUS_TO_BE_REMOVED;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isGoingToBeRemoved() {
        return this.status.equals(WaitListRecord.STATUS_TO_BE_REMOVED);
    }

    public boolean isFinalized() {
        return this.status.equals(WaitListRecord.STATUS_FINALIZED);
    }

    public static WaitListRecord getFromResultSet(ResultSet resultSet) throws SQLException {
        return new WaitListRecord(
            Student.getFromResultSet(resultSet),
            resultSet.getString("status")
        );
    }


}
