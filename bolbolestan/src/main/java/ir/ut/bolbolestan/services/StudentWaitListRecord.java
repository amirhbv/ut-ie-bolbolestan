package ir.ut.bolbolestan.services;

import ir.ut.bolbolestan.database.DatabaseHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class StudentWaitListRecord {
    public static String STATUS_PENDING = "pending";
    public static String STATUS_FINALIZED = "finalized";
    public static String STATUS_TO_BE_REMOVED = "toBeRemoved";

    private final Offering offering;
    private final WaitListRecord record;

    public Offering getOffering() {
        return offering;
    }

    public String getStatus() {
        return this.record.getStatus();
    }

    public WaitListRecord getWaitListRecord() {
        return record;
    }

    public StudentWaitListRecord(Offering offering, WaitListRecord record) {
        this.offering = offering;
        this.record = record;
    }

    public void finalizeRecord() {
        if (this.record.isGoingToBeRemoved()) {
            deleteWaitListRecordFromDb();
            this.offering.decreaseSignedUp();
        }
        else if (!this.record.isFinalized()) {
            updateStatusInDb(WaitListRecord.STATUS_FINALIZED);
            this.offering.increaseSignedUp();
        }
    }

    public void deleteWaitListRecordFromDb() {
        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM WeeklyScheduleOffering " +
                    "WHERE offeringCode = ? AND offeringClassCode = ? AND  studentId = ? "
            );
            statement.setString(1, offering.getCode());
            statement.setString(2, offering.getClassCode());
            statement.setString(3, record.getStudent().getId());
            statement.executeUpdate();
            DatabaseHandler.closeStatement(connection, statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateStatusInDb(String status) {
        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                "UPDATE WeeklyScheduleOffering SET status = ? " +
                    "WHERE offeringCode = ? AND offeringClassCode = ? AND studentId = ?"
            );
            statement.setString(1, status);
            statement.setString(2, offering.getCode());
            statement.setString(3, offering.getClassCode());
            statement.setString(4, record.getStudent().getId());
            statement.executeUpdate();
            DatabaseHandler.closeStatement(connection, statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
