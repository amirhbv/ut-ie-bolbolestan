package ir.ut.bolbolestan.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.json.JSONArray;
import org.json.JSONObject;

import ir.ut.bolbolestan.database.DatabaseHandler;
import ir.ut.bolbolestan.exception.ClassTimeCollisionException;
import ir.ut.bolbolestan.exception.ExamTimeCollisionException;
import ir.ut.bolbolestan.exception.OfferingNotFoundException;
import ir.ut.bolbolestan.exception.WrongOfferingTypeException;

public class Offering {
    private final String code;
    private final String classCode;
    private final String name;
    private final String instructor;
    private final String type;
    private final int units;
    private final ClassTime classTime;
    private final ExamTime examTime;
    private int capacity;
    private int signedUp;
    private final ArrayList<String> prerequisites;

    public static ArrayList<String> ALLOWED_TYPES = new ArrayList<>(List.of(
        "Asli", "Paaye", "Takhasosi", "Umumi"
    ));

    public Offering(String code, String classCode, String name, String instructor, int units, String type, ClassTime classTime, ExamTime examTime,
            int capacity, ArrayList<String> prerequisites) {
        this(code, classCode, name, instructor, units, type, classTime, examTime, capacity, prerequisites, 0);
    }

    public Offering(String code, String classCode, String name, String instructor, int units, String type,
            ClassTime classTime, ExamTime examTime, int capacity, ArrayList<String> prerequisites, int signedUp) {
        this.code = code;
        this.classCode = classCode;
        this.name = name;
        this.instructor = instructor;
        this.units = units;
        this.type = type;
        this.classTime = classTime;
        this.examTime = examTime;
        this.capacity = capacity;
        this.signedUp = signedUp;
        this.prerequisites = prerequisites;
    }

    public String getName() {
        return this.name;
    }

    public String getInstructor() {
        return this.instructor;
    }

    public String getUniqueCode() {
        return this.code + this.classCode;
    }

    public String getCode() {
        return this.code;
    }

    public String getClassCode() {
        return this.classCode;
    }

    public ClassTime getClassTime() {
        return this.classTime;
    }

    public ExamTime getExamTime() {
        return this.examTime;
    }

    @JsonIgnore
    public ArrayList<String> getPrerequisiteCodes() {
        return this.prerequisites;
    }

    public ArrayList<String> getPrerequisites() {
        ArrayList<String> result = new ArrayList<>();

        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {

            PreparedStatement statement = connection.prepareStatement("SELECT name FROM Course WHERE code IN (?)");
            statement.setString(1, String.join(",", this.prerequisites));

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(resultSet.getString("name"));
            }

            DatabaseHandler.closeResultSet(connection, statement, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public void checkCollision(Offering offering) throws ClassTimeCollisionException, ExamTimeCollisionException {
        checkClassTimeCollision(offering);
        checkExamTimeCollision(offering);
    }

    private void checkClassTimeCollision(Offering offering) throws ClassTimeCollisionException {
        if (this.getClassTime().hasOverlap(offering.getClassTime())) {
            throw new ClassTimeCollisionException(this.getCode(), offering.getCode());
        }
    }

    private void checkExamTimeCollision(Offering offering) throws ExamTimeCollisionException {
        if (this.getExamTime().hasOverlap(offering.getExamTime())) {
            throw new ExamTimeCollisionException(this.getCode(), offering.getCode());
        }
    }

    public int getUnits() {
        return this.units;
    }

    public Boolean hasRemainingCapacity() {
        return this.capacity - this.signedUp > 0;
    }

    public int getSignedUpCount() {
        return this.signedUp;
    }

    public int getCapacity() {
        return this.capacity;
    }

    public String getType() {
        return this.type;
    }

    public void increaseSignedUp() {
        this.signedUp++;
    }

    public void decreaseSignedUp() {
        this.signedUp--;
    }

    public void increaseCapacity() {
        this.capacity++;
    }

    public static Offering deserialize(JSONObject jsonObject) {
        ArrayList<String> prerequisites = new ArrayList<>();
        JSONArray jArray = (JSONArray) jsonObject.get("prerequisites");
        if (jArray != null) {
            for (int i = 0; i < jArray.length(); i++) {
                prerequisites.add(jArray.getString(i));
            }
        }

        return new Offering(jsonObject.getString("code"), jsonObject.getString("classCode"),
                jsonObject.getString("name"), jsonObject.getString("instructor"),
                jsonObject.getInt("units"), jsonObject.getString("type"),
                ClassTime.deserialize(jsonObject.getJSONObject("classTime")),
                ExamTime.deserialize(jsonObject.getJSONObject("examTime")), jsonObject.getInt("capacity"),
                prerequisites);
    }

    private void addCourseToDB() {
        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO Course " +
                "(code, name, type, units) " +
                "VALUES (?, ?, ?, ?)"
            );
            statement.setString(1, this.code);
            statement.setString(2, this.name);
            statement.setString(3, this.type);
            statement.setInt(4, this.units);

            statement.executeUpdate();
            DatabaseHandler.closeStatement(connection, statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addOfferingToDB() {
        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO Offering " +
                "(code, classCode, instructor, classTime, classDays, examStart, examEnd, capacity) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
            );
            statement.setString(1, this.code);
            statement.setString(2, this.classCode);
            statement.setString(3, this.instructor);
            statement.setString(4, this.classTime.getTimeString());
            statement.setString(5, this.classTime.getDaysString());
            statement.setString(6, this.examTime.getStart());
            statement.setString(7, this.examTime.getEnd());
            statement.setInt(8, this.capacity + 5);

            statement.executeUpdate();
            DatabaseHandler.closeStatement(connection, statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addToDB() {
        this.addCourseToDB();
        this.addOfferingToDB();
    }

    public void addPrerequisitesToDB() {
        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
            connection.setAutoCommit(false);

            for (String prerequisite : this.prerequisites) {
                PreparedStatement statement = connection
                        .prepareStatement("INSERT INTO Prerequisite " + "(course, prerequisite) " + "VALUES (?, ?)");

                statement.setString(1, this.code);
                statement.setString(2, prerequisite);

                statement.executeUpdate();
                statement.close();
            }

            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, Object> getCourseByCode(String code) throws OfferingNotFoundException {
        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Course WHERE code = ?");
            statement.setString(1, code);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Map<String, Object> result = Map.of(
                    "name", resultSet.getString("name"),
                    "units", resultSet.getInt("units")
                );
                DatabaseHandler.closeResultSet(connection, statement, resultSet);
                return result;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        throw new OfferingNotFoundException(code);
    }

	public static Offering getByCode(String uniqueCode) throws OfferingNotFoundException {
        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {

            String code = uniqueCode.substring(0, 7);
            String classCode = uniqueCode.substring(7);
            PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM Offering O INNER JOIN Course C ON C.code = O.code WHERE O.code = ? AND O.classCode = ?");
            statement.setString(1, code);
            statement.setString(2, classCode);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Offering result = Offering.getFromResultSet(resultSet);
                DatabaseHandler.closeResultSet(connection, statement, resultSet);
                return result;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        throw new OfferingNotFoundException(uniqueCode);
	}

    private static ArrayList<String> getPrerequisitesByCode(String code) {
        ArrayList<String> result = new ArrayList<>();

        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Prerequisite WHERE course = ?");
            statement.setString(1, code);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(resultSet.getString("prerequisite"));
            }

            DatabaseHandler.closeResultSet(connection, statement, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static Offering getFromResultSet(ResultSet resultSet) throws SQLException {
        String code = resultSet.getString("code");
        String classCode = resultSet.getString("classCode");

        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {
            PreparedStatement statement = connection
                    .prepareStatement("SELECT COUNT(*) AS count FROM WeeklyScheduleOffering WHERE offeringCode = ? AND offeringClassCode = ?");
            statement.setString(1, code);
            statement.setString(2, classCode);

            ResultSet countResultSet = statement.executeQuery();
            countResultSet.next();
            int signedUpCount = countResultSet.getInt("count");

            DatabaseHandler.closeResultSet(connection, statement, countResultSet);

            return new Offering(
                code,
                classCode,
                resultSet.getString("name"),
                resultSet.getString("instructor"),
                resultSet.getInt("units"),
                resultSet.getString("type"),
                ClassTime.getFromResultSet(resultSet),
                ExamTime.getFromResultSet(resultSet),
                resultSet.getInt("capacity"),
                Offering.getPrerequisitesByCode(code),
                signedUpCount
            );
        } catch (SQLException e) {
            throw e;
        }
    }

    public static ArrayList<Offering> getListFromDB(String type, String searchedPhrase) throws WrongOfferingTypeException {
        if (type != null && !Offering.ALLOWED_TYPES.contains(type)) {
            throw new WrongOfferingTypeException();
        }

        ArrayList<Offering> result = new ArrayList<>();


        try (Connection connection = DatabaseHandler.getDataSource().getConnection()) {

            PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM Offering O INNER JOIN Course C ON C.code = O.code " +
                "WHERE (type = ? OR ? IS NULL) AND (name LIKE ? OR ? IS NULL)" +
                "ORDER BY name ASC"
            );
            statement.setString(1, type);
            statement.setString(2, type);
            statement.setString(3, "%" + searchedPhrase + "%");
            statement.setString(4, searchedPhrase);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(getFromResultSet(resultSet));
            }

            DatabaseHandler.closeResultSet(connection, statement, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
	}
}
