package ir.ut.bolbolestan.services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class ClassTime {
    private ArrayList<String> days;
    private Time start;
    private Time end;

    public ClassTime(ArrayList<String> days, Time start, Time end) {
        this.days = days;
        this.start = start;
        this.end = end;
    }

    public ArrayList<String> getDays() {
        return this.days;
    }

    public Map<String, String> getTime() {
        return Map.of(
            "start", this.start.toString(),
            "end", this.end.toString()
        );
    }

    public String getTimeString() {
        return String.format("%s-%s", this.start, this.end);
    }

    public String getDaysString() {
        return String.join(",", this.days);
    }

    public static ClassTime deserialize(JSONObject jsonObject) {
        ArrayList<String> days = new ArrayList<>();
        JSONArray jArray = (JSONArray) jsonObject.get("days");
        if (jArray != null) {
            for (int i = 0; i < jArray.length(); i++) {
                days.add(jArray.getString(i).toLowerCase());
            }
        }

        String[] parts = jsonObject.getString("time").split("-");
        String start = parts[0];
        String end = parts[1];

        return new ClassTime(
            days,
            Time.buildFromString(start),
            Time.buildFromString(end)
        );
    }

    public Boolean hasOverlap(ClassTime other) {
        if (this.start.isBefore(other.end) && other.start.isBefore(this.end)) {
            for (String day : this.days) {
                if (other.days.contains(day)) {
                    return true;
                }
            }
        }

        return false;
    }

	public static ClassTime getFromResultSet(ResultSet resultSet) throws SQLException {
        String[] parts = resultSet.getString("classTime").split("-");
        String start = parts[0];
        String end = parts[1];

        return new ClassTime(
            new ArrayList<String>(Arrays.asList(resultSet.getString("classDays").split("-"))),
            Time.buildFromString(start),
            Time.buildFromString(end)
        );
	}
}
