package ir.ut.bolbolestan.services;

import ir.ut.bolbolestan.Bolbolestan;

public class WaitListTask implements Runnable {

    @Override
    public void run() {
        Bolbolestan.getInstance().processWaitList();
    }
}