package ir.ut.bolbolestan.services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;

public class ExamTime {
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private Date start;
    private Date end;

    public ExamTime(Date start, Date end) {
        this.start = start;
        this.end = end;
    }

    public String getStart() {
        return ExamTime.dateFormat.format(this.start);
    }

    public String getEnd() {
        return ExamTime.dateFormat.format(this.end);
    }

    public static ExamTime deserialize(JSONObject jsonObject) {
        Date start = new Date(), end = new Date();

        try {
            start = ExamTime.dateFormat.parse(jsonObject.getString("start"));
            end = ExamTime.dateFormat.parse(jsonObject.getString("end"));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new ExamTime(start, end);
    }

	public Boolean hasOverlap(ExamTime other) {
		return this.start.before(other.end) && other.start.before(this.end);
	}

	public static ExamTime getFromResultSet(ResultSet resultSet) throws SQLException {
        return new ExamTime(resultSet.getDate("examStart"), resultSet.getDate("examEnd"));
	}
}
