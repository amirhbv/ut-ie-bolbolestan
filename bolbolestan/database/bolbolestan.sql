drop database if exists bolbolestan;
create database bolbolestan character set UTF8 collate utf8_bin;

drop user if exists bolbolestan@localhost;
create user 'bolbolestan'@'localhost' IDENTIFIED BY 'bolbolestanpassword';
GRANT ALL PRIVILEGES ON bolbolestan.* TO 'bolbolestan'@'localhost';

use bolbolestan;

create table Student
(
    id varchar(30) not null,
    name varchar(20) null,
    secondName varchar(20) null,
    birthDate varchar(20) not null,
    field varchar(50) null,
    faculty varchar(30) not null,
    level varchar(30) not null,
    status varchar(30) not null,
    img varchar(200) not null,
    email varchar(100) not null UNIQUE,
    password varchar(256) not null,

    constraint student_pk
        primary key (id)
);

create table Course
(
    code varchar(10) not null,
    type enum('Asli', 'Paaye', 'Takhasosi', 'Umumi') not null,
    units int not null,
    name varchar(50) not null,

    constraint course_pk
        primary key (code)
);

create table Offering
(
    code varchar(10) not null,
    classCode varchar(2) not null,
    instructor varchar(50) not null,
    classTime varchar(100) not null,
    classDays varchar(100) not null,
    examStart datetime not null,
    examEnd datetime not null,
    capacity int not null,

    constraint offering_pk
        primary key (code, classCode),

    constraint offering_course_fk
    foreign key (code)
        references Course (code)
        on delete cascade
);

create table Prerequisite
(
    course varchar(20),
    prerequisite varchar(20),

    constraint prerequisite_pk
        primary key (course, prerequisite),

    constraint prerequisite_offering_fk
    foreign key (course)
        references Course (code)
        on delete cascade,

    constraint prerequisite_prerequisite_fk
    foreign key (prerequisite)
        references Course (code)
        on delete cascade
);

create table TakenCourse
(
    studentId varchar(30) not null,
    code varchar(10) not null,
    grade float not null,
    term int not null,
    status varchar(30) not null,

    constraint takenCourse_pk
        primary key (studentId, term, code),

    constraint takenCourse_course_fk
    foreign key (code)
        references Course (code)
        on delete cascade,

    constraint takenCourse_student_fk
    foreign key (studentId)
        references Student (id)
        on delete cascade
);

create table WeeklyScheduleOffering
(
    id int auto_increment,
    offeringCode varchar(20) not null,
    offeringClassCode varchar(2) not null,
    studentId varchar(20) not null,
    status enum('pending', 'finalized', 'toBeRemoved') not null,
    waitList boolean default false,

    constraint wso_pk
        primary key (id),

    constraint wso_student_fk
    foreign key (studentId)
        references Student (id)
        on delete cascade,

    constraint wso_offering_fk
    foreign key (offeringCode, offeringClassCode)
        references Offering (code, classCode)
        on delete cascade
);
